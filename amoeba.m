% amoeba.m
% Derived from Numerical Recipes, Press et al.
% Author: Michael Grabe
% See LICENSE for copyright and license info.

function [E,p_best]=amoeba(p_init,model,a,score,tol)

ftol = 1e-6;
if nargin > 4
  ftol = tol;
end

if nargin < 4
  score = @score_pit;
end  % otherwise, use the user supplied function to score the fit

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AMOEBA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%warning off
itmax=100;         % number of maximum iterations

% np = number of parameters to be fit
% mp = number of initial guesses
[mp,np] = size(p_init);

alpha=1;           % 1
beta=0.5;          % 0.5
gamma=2;           % 2

% initialize the parameters
[y]=initial(p_init,mp,model,a,score);
p=p_init;
% p is an array of initial guesses (mp,np) in size
% y is the evaluation of the points in P (mp) in size

iter=0;
ilo=3;  % randomly assign this at first

done=0;
error=0;

while done == 0,
   if y(1) > y(2), % randomly assign highest and next highest
      ihi=1;
      inhi=2;

   else
      ihi=2;
      inhi=1;
   end
   for i=1:mp,
      if y(i) < y(ilo),
         ilo=i;
      end
      if y(i) > y(ihi),
         inhi=ihi;
         ihi=i;
      elseif y(i)> y(inhi),
         if 	i ~= ihi,
            inhi =i;
         end
      end
   end
   rtol=2*abs(y(ihi)-y(ilo))/(abs(y(ihi)+y(ilo)));
   if rtol < ftol,
      done=1;  % the search is complete
   end
   if iter == itmax,
      done=1;  % the program stops and we get an error flag
      error=1;
   end
   iter=iter+1;
   for j=1:np,
      pbar(j)=0;
   end
   for i=1:mp,
      if i ~= ihi,


         for j=1:np,
            pbar(j)=pbar(j)+p(i,j);
         end
      end
   end
   for j=1:np,
      pbar(j)=pbar(j)/np;
      pr(j)=(1+alpha)*pbar(j)-alpha*p(ihi,j);
   end
   [ypr]=score(pr,model,a);
   if ypr <= y(ilo),
      for j=1:np,
         prr(j)=gamma*pr(j) + (1-gamma)*pbar(j);
      end
      [yprr]=score(prr,model,a);
      if yprr < y(ilo),
         for j=1:np,
            p(ihi,j)=prr(j);
         end
         y(ihi)=yprr;
      else
         for j=1:np,
            p(ihi,j)=pr(j);
         end
         y(ihi)=ypr;
      end
   elseif  ypr >= y(inhi),
      if ypr < y(ihi),

        for j=1:np,
            p(ihi,j)=pr(j);
         end
         y(ihi)=ypr;
      end
      for j=1:np,
         prr(j) = beta*p(ihi,j)+(1-beta)*pbar(j);
      end
      [yprr]=score(prr,model,a);
      if yprr < y(ihi),
         for j=1:np,
            p(ihi,j)=prr(j);
         end
      else
         for i=1:mp,
            if i ~= ilo,
               for j=1:np,
                  pr(j)=.5*(p(i,j) +p(ilo,j));
                  p(i,j) = pr(j);
               end
               [y(i)]=score(pr,model,a);
            end
         end
      end
   else
      for j=1:np,
         p(ihi,j)=pr(j);
      end
      y(ihi)=ypr;
   end

end

[E,index]=min(y);
[p_best]=p(index,:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [err] = score_pit(p,model,a)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% p are the parameters to be fit
% a is the data a(:,1) - time, a(:,2) - pH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
err=0;
if min(p) <= 0
err=1000;
else
% call on the ODE solver with the data set
[tn,y]=ph_setup(p,model);
ph_p = y(:,12);

yi=interp1(tn,ph_p,a(:,1));           % interpolate pH to the points
err=sum(abs(a(:,2)-yi));
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y]=initial(p_best,mp,model,a,score)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% this is the initializing function for the AMOEBA program.
% score these initial guesses
for i=1:mp
    [y(i)]=score(p_best(i,:),model,a);
end
