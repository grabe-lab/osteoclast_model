%-------------------------------------------------------------------------%
% enclosed charge [#]
%-------------------------------------------------------------------------%
% Find the charge enclosed by a membrane in number of elementary charges.
%
% u     - the reduced potential on the membrane in units of kT/e
% C0    - membrane capacitance per unit area [C/mV/cm^2]
% S     - membrane surface area [cm^2]
%
% This function should be the inverse of reduced_potential.m
%-------------------------------------------------------------------------%

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README

function q = enclosed_charge(u, C0, S)
    physical_constants;

    q = u * C0 * S * kTe / e;
end
