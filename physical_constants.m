% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

NA   = 6.02214129e23;      % Avogadro's number [1/mol]
e    = 1.602176565e-19;    % Elementary charge [C]
F    = 9.64853399e4;       % Faraday constant [C/mol] = eq * NA
kB   = 8.6173324e-5;       % Boltzmann's constant [eV/K]

T25C = 298.15;             % [K], 25 degrees C = standard ambient temperature
kTe  = 2.56925765506e+01;  % kB * T25C * 1000 = kB T / e = 25.69 mV
