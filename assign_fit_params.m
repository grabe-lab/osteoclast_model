% ASSIGN_FIT_PARAMS
%   model = ASSIGN_FIT_PARAMS(params,model)
%
% This is a small utility function which may be useful
% after running a fit to update the model parameters to
% in model use the fit values in params
%
% Then you only need the model, which makes it easier
% to modify parameters and rerun or plot.
%
% If model.fit = {'N_v','N_clc','V_p'} and p = [1e5 1e6 3e-18],
% then mod = assign_fit_params(p,model) returns a model with:
% mod.N_v set to 1e5,
% mod.N_clc set to 1e6, and 
% mod.V_p set to 3e-18.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = assign_fit_params(p,model)

    if ~isfield(model,'fit')
        error('model.fit not set');
    end

    if length(p) ~= length(model.fit)
        error('p and model.fit must be the same length');
    end

    for k = 1:length(p)
        model.(model.fit{k}) = p(k);
    end
end
