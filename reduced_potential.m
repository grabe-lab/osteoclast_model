%-------------------------------------------------------------------------%
% membrane potential
%-------------------------------------------------------------------------%
% This quantity is a REDUCED membrane potential in units of kT/e.
%
% q     - mobile charges inside the membrane [#]
% C0    - membrane capacitance per unit area [C/mV/cm^2]
% S     - membrane surface area [cm^2]
%
% This function should be the inverse of enclosed_charge.m
%-------------------------------------------------------------------------%

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README

function u = reduced_potential(q, C0, S)
    physical_constants;

    u = e/(C0*S*kTe)*q;
end
