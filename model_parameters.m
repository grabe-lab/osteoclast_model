function p = model_parameters()

physical_constants;

%-----------------------VATPase pump file---------------------------------%
p.pump = 'v_0003';      % load file p.pump + '.mat'

%-----------------------Geometry Parameters-------------------------------%
p.V_c = 850e-18;        % cytoplasm volume [m^3]
p.S_c = 400e-08;        % cytoplasm surface [cm^2]
p.V_p = 100e-18;        % pit volume [m^3]
p.S_p = 650e-08;        % pit surface area [cm^2]

p.C0  = 1e-9;           % capacitance of membrane C/(mV*cm**2)

%-----------------------Perm Cl, K ---------------------------------------%
p.P_cl_p  = 1e-8;       % pit  Chloride permeability  [cm/s]
p.P_cl_c  = 1e-8;       % cyto Chloride permeability  [cm/s]
p.P_k_p   = 7.1e-7;     % pit  Potassium permeability [cm/s]
p.P_k_c   = 7.1e-7;     % cyto Potassium permeability [cm/s]
p.P_na_p  = 1.92e-8;    % pit  Sodium permeability [cm/s]
p.P_na_c  = 1.92e-8;    % cyto Sodium permeability [cm/s]
p.P_h_p   = 6e-5;       % proton permeability [cm/s], 6e-5 from Ishida et al, 2013.
p.P_nh3_c = 48e-3;      % Antonenko et al, Biophys. J. V72 May 1997, 2187-2195

%-----------------------Ion concentration and pH parameters---------------%
p.Psi_c  = -70;         % initial cytoplasm - extracellular voltage [mV]

p.ph_c   = 7.25;        % cytoplasm pH
p.ph_e   = 7.4;         % extracellular pH
p.ph_p   = 7.26;        % pit pH      (7.05 for panel a, 7.2 for panel c)

p.cl_c   = 0.010;       % cytoplasm [M]
p.cl_e   = 0.110;       % extracellular [M]
p.cl_p   = 0.110;       % pit [M]

p.k_c    = 0.145;                       % cytoplasm [M]
p.k_e    = p.k_c * exp(p.Psi_c / kTe);  % extracellular [M]
p.k_p    = p.k_e;                       % pit [M]

                        % initial pit potential, set near fast channel equilibrium
p.Psi_p  = kTe * log((p.P_k_p * p.k_c + p.P_cl_p * p.cl_p)/...
                     (p.P_k_p * p.k_p + p.P_cl_p * p.cl_c));

p.na_c   = 0.010;       % cytoplasm [M]
p.na_e   = 0.145;       % extracellular [M]
p.na_p   = 0.145;       % pit [M]

p.hco3_c = 0.024;       % cytoplasm [M]
p.hco3_e = 0.024;       % extracellular [M]

p.nh4_c  = 0;           % [M]
p.nh3_c  = 0;           % [M]
p.nh3_e  = 0;

%-----------------------Optimized parameters (from amoeba)----------------%
p.N_v   = 11091;        % N V-ATPases
p.N_clc = 7027;         % number ClC-7
p.N_ae2 = 1e5;          % N AE2 exchangers
%-----------------------Not yet optimized parameters----------------------%
p.N_nhe = 1e5;          % N Sodium-proton exchangers (made up. FIX)
p.N_nak = 1;            % N Na+/K+-ATPases. (made up. FIX)
p.N_hv1_e = 5e5;        % N Hv1 pumps on extracellular membrane
p.N_hv1 = p.N_hv1_e * p.S_p / p.S_c;  % N Hv1 pumps on ruffled border

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-----------------------Cytoplasmic bicarbonate parameters ---------------%
p.co2     = 5.24e-3;    % CO2 concentration [M]
p.koff    = 0.48e+6;    % CO2 hydration backward rate constant [M^-1 s^-1]
p.kon     = 0.365;      % CO2 hydration forward rate constant [s^-1]
% set bicarb equilibrium to initial conditions
%p.kon     = p.koff * p.hco3_c * 10^-p.ph_c / p.co2;

%-----------------------Other buffer species------------------------------%
% Add in NH3/NH4+ buffer
pKa_nh3 = 9.25;
p.nh3_kon = 1e9;         % [1/M/s]   I have not been able to find rates yet.
p.nh3_koff = p.nh3_kon * 10^-pKa_nh3; % [1/s]

% Add some closed-system buffers to each compartment.
% For a single closed-system buffer, max(beta(pH)) = beta(pK) = log(10)[T]/4,
% where [T] is the total number of closed-system buffer molecules.
% beta(pH) has a HWHM of log10(3+sqrt(8)) ~= 0.766.
% Maximum beta is about 2.27058 log(10) [T] / 4 in the middle of
% a series of buffers spread about 0.766 pH apart.
% So, constantish beta = 40 mM/pH means [T] of about 31 mM/pH.
%
% Summing up contributions from buffers in either direction around
% the peak of a buffer:
% 2.27058 = 1 + 2 * Sum[4(3+Sqrt[8])^k/(1+(3+Sqrt[8])^k)^2,{k,1,Infinity}]
% Obviously Infinity is a bit drastic, but the series converges very rapidly.
pKB = transpose(2:log10(3+sqrt(8)):9);
nB = length(pKB);
p.Bon_p = 1e8 + zeros(nB,1);    % [M^-1 s^-1]
p.Bon_c = 1e8 + zeros(nB,1);    % arbitrary, faster than bicarb
p.Boff_p  = p.Bon_p .* 10.^-pKB; % [s^-1]
p.Boff_c  = p.Bon_c .* 10.^-pKB;
scale    = 4/log(10)/2.27058;    % if TB = x * scale, then max beta == x
p.TB_p   = 0.025*scale + zeros(nB,1);  % [M]
p.TB_c   = 0.040*scale + zeros(nB,1);
p.B_p    = p.TB_p ./ (1 + 10.^(pKB-p.ph_p));
p.B_c    = p.TB_c ./ (1 + 10.^(pKB-p.ph_c));

%-----------------------Set active/inactive transporters------------------%
p.t_hleak_p   = 0;      % Turn any term on or off at any time.
p.t_clleak_p  = 0;      % Set to -Inf through 0 to always be active.
p.t_kleak_p   = 0;      % Set to tfinal through Inf to never be active.
p.t_kleak_e   = 0;      % Set between 0 and tfinal to turn on mid-run.
p.t_clleak_e  = Inf;
p.t_naleak_p  = Inf;
p.t_naleak_e  = Inf;
p.t_nh3leak_e = 0;
p.t_hco3      = 0;
p.t_hv1       = 0;
p.t_clc       = 0;
p.t_ae2       = 0;
p.t_nhe       = 0;
p.t_nak       = 0;
p.t_vatp      = 5250;
p.t_hv1_e     = Inf;
p.t_B_p       = 0;      % Buffering by explicit, closed system buffers
p.t_B_c       = 0;
p.t_nh3_c     = 0;
p.t_nh3_e     = Inf;

%-----------------------Add flux of ions----------------------------------%
% This creates a constant flux of strength x between time t1 and t2:
% struct('t',[t1 t2],'x',x,'f',@(t,x)(x));
% The flux function can of course be much more complicated.
% t must be a length 2 array. x can be any arbitrary thing that
% your flux function is designed to accept.
fs = struct('t',[Inf Inf],'x',0,'f',@(t,x)(x));
p.fh_p       = fs;
p.fcl_p      = fs;
p.fk_p       = fs;
p.fna_p      = fs;
p.fh_c       = fs;
p.fcl_c      = fs;
p.fk_c       = fs;
p.fna_c      = fs;
p.fhco3_c    = fs;
p.fD_p       = fs;      % Add negative impermeant ions to pit
p.fD_c       = fs;      % Add negative impermeant ions to cytoplasm

p.fnh3_e     = fs;      % this is used for smoothly turning off the external NH3

%-----------------------Set simulation time-------------------------------%
p.tstart     = 0;       % Start time [s]
p.tfinal     = 32768;   % Stop time [s]
p.tstep      = 8;       % Time step [s]

%-----------------------Ode solver options--------------------------------%
p.abstol     = 1e-6;    % absolute tolerance
p.reltol     = 1e-6;    % relative tolerance

%-----------------------odds and ends-------------------------------------%
p.plot       = false;   % true -> plot the results
p.currents   = false;   % true -> record each individual current
p.verbose    = true;    % false -> suppress printing of solver status

end
