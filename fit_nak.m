% FIT_NAK
%   model.N_nak = fit_nak(model)
%
% Find the value of N_nak which holds [K+]_C roughly constant
% over the duration of the model.  Bit of a hack.
% Not what you always want to do.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%


function N_nak = fit_nak(model,high)
    if nargin < 2
        high = 40;
    end

    mod = model;

    for k = 1:length(model);
        mod(k).verbose = false;
        mod(k).plot = false;
        mod(k).currents = false;
        mod(k).fit = {'N_nak'};
    end

    N_nak = find_root2(0,high,mod,1e-3);
end

function err = score_k(p,model)

    [~,y] = ph_setup(p,model);
    [nt,np] = size(y);
    if nt == 1 || np == 1 || any(isinf(y(end,:))) || p < 0
        err = 1e10;
    else
        k_c = get_concentration('k_c',y,model);
        err = k_c(end) - model(1).k_c;
    end
end


function mid = find_root(low,high,mod,tol)

    bad = 1e10;

    elow = score_k(low,mod);
    ehigh = score_k(high,mod);

    if sign(elow) == sign(ehigh)
        if elow == bad && ehigh == bad
            error('bad endpoints (%f, %f)\n', low, high);
        else
            fprintf('root not in range %f to %f.  expanding.\n',low,high);
            low = 0;
            elow = score_k(low,mod);

            high = high * 10;
            ehigh = score_k(high,mod);
            if elow == bad && ehigh == bad
                error('did not help');
            end
        end
    end

    fprintf('root: %f,  error: %f\n',low,elow);
    fprintf('root: %f,  error: %f\n',high,ehigh);

    mid = (low+high)/2;
    err = score_k(mid,mod);
    fprintf('root: %f,  error: %f\n',mid,err);

    while abs(err) > tol

        if sign(elow) == sign(err)
            low = mid;
            elow = err;
        else
            high = mid;
            ehigh = err;
        end

        mid = (low + high)/2;

        err = score_k(mid,mod);
        fprintf('root: %f,  error: %f\n',mid,err);
    end
end

function mid = find_root2(low,high,mod,tol)

    bad = 1e10;

    elow = score_k(low,mod);
    ehigh = score_k(high,mod);

    fprintf('root: %f,  error: %f\n',low,elow);
    fprintf('root: %f,  error: %f\n',high,ehigh);

    if sign(elow) == sign(ehigh)
        [mod.verbose] = deal(true);
        ph_setup((low+high)/2,mod);

        warning('root not contained in range');
        mid = Inf;
        return
    end

    err = bad;
    rel = bad;

    while abs(err) > tol && rel > tol

        if ehigh < bad && elow < bad
            % linearly interpolate to the root
            mid = (low*ehigh - high*elow) / (ehigh - elow);
        else
            mid = (high + low)/2;
        end

        err = score_k(mid,mod);
        
        if err < bad
            rel = min(abs(elow-err),abs(ehigh-err));
        else
            rel = bad;
        end
            
        if sign(elow) == sign(err)
            low = mid;
            elow = err;
        else
            high = mid;
            ehigh = err;
        end

        fprintf('root: %f,  error: %f, relative error: %f\n',mid,err,rel);
    end

end

