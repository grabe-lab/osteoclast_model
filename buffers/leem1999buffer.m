% Replace the current cytoplasmic intrinsic buffer with
% the cytoplasmic buffer of guinea pig ventricular myocytes
% from Leem et al., Journal of Physiology (1999), 517.1, pp.159—180.
%
% This model is valid from about pH 6.2 to 7.7

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = leem1999buffer(model)
    model.TB_c = [0.08422 0.02938]; % [M]
    model.Bon_c = [1e8 1e8];  % [M^-1 s^-1] arbitrary, but pretty fast
    pK = [6.03 7.57];
    K = 10.^-pK;
    H = 10.^-model.ph_c;
    model.Boff_c = K .* model.Bon_c;
    model.B_c = model.TB_c .* K ./ (K + H);
end
