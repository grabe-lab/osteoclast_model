% model = SET_BICARB_PH(model,ph)
%
% The literature values for [C02], kon and koff are:
% p.co2     = 5.24e-3;    % CO2 concentration [M]
% p.koff    = 0.48e+6;    % CO2 hydration backward rate constant [s^-1]
% p.kon     = 0.365;      % CO2 hydration forward rate constant [s^-1]
%
% We don't want to change their values in model_parameters.m,
% so here is a helper function to move the equilibrium point,
% so that at some input pH and initial [HCO3-] level,
% bicarb is in equilibrium.
%
% We do this by modifying [CO2].

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%


function model = set_bicarb_ph(model,ph)

    alpha = model(1).koff * model(1).hco3_c * (10^-ph) / ...
            (model(1).co2 * model(1).kon);

    for k = 1:length(model)
        model(k).co2  = model(k).co2  * alpha;
    end
end
