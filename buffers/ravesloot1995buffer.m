% Replace the current cytoplasmic intrinsic buffer with
% the cytoplasmic buffer of rat osteoclasts from:
%
% See figure 7 of:
%   Jan H. Ravesloot, Thomas Eisen, Roland Baron, and Walter F. Boron,
%   Role of Na-H Exchangers and Vacuolar H+ Pumps in Intracellular pH
%   Regulation in Neonatal Rat Osteoclasts, J. Gen. Physiol. 1995 105:177-208.
%
% This model is valid from about pH 6.41 to 7.36
% Numbers were taken from:

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = ravesloot1995buffer(model)
    model.TB_c = 0.047; % [M]
    model.Bon_c = 1e8;  % [M^-1 s^-1] arbitrary, but pretty fast
    pK = 6.34;
    K = 10.^-pK;
    H = 10.^-model.ph_c;
    model.Boff_c = K .* model.Bon_c;
    model.B_c = model.TB_c .* K ./ (K + H);
end
