% SET_BUFFERS
%    new_model = SET_BUFFERS(model,pK_p,TB_p,Bon_p,pK_c,TB_c,Bon_c)
%
%    Replace the explicit intrinsic buffers in model,
%    and equilibrate them.
%    pK_p and pK_c are vectors of buffer pKa values,
%    TB_p and TB_c are concentrations of total buffer molecules, and
%    Bon_p and Bon_c are acid association rate constants.
%
%    pK_p, TB_p and Bon_p should all be the same sinze
%
%    [TB] --(Boff)--> [B][H]
%    [TB] <--(Bon)--- [B][H]
%
%    pKa = -log10 ( Boff ./ Bon )

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = set_buffers(model,pK_p,TB_p,Bon_p,pK_c,TB_c,Bon_c)
    model.Bon_p = Bon_p;
    model.Bon_c = Bon_c;

    model.Boff_p = Bon_p .* 10.^-pK_p;
    model.Boff_c = Bon_c .* 10.^-pK_c;

    model.TB_p = TB_p;
    model.TB_c = TB_c;

    % now set model.B_p and model.B_c for the current pH.
    model = equilibrate_buffers(model);
end
