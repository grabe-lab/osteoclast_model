% Set the approximate constant beta for the pit and cytoplasm
% to p and c, assuming the model contains a series of closed-system
% buffers spaced about log10(3+sqrt(8)) pH apart.
%
% if p or c is <= 0, then that buffer is not modified

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = set_beta(model,p,c)
    scale = 4/log(10)/2.27058;   % if TB = x * scale, then max beta ~= x

    if p > 0
        model.TB_p(:) = scale * p;
        K_p = (model.Boff_p ./ model.Bon_p);
        H_p = 10^-model.ph_p;
        model.B_p = model.TB_p .* K_p ./ (K_p + H_p);
    end

    if c > 0
        model.TB_c(:) = scale * c;
        K_c = (model.Boff_c ./ model.Bon_c);
        H_c = 10^-model.ph_c;
        model.B_c = model.TB_c .* K_c ./ (K_c + H_c);
    end
end
