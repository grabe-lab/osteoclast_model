% EQUILIBRATE_BUFFERS
%
%     new_model = EQUILIBRATE_BUFFERS(model)
%
%   Given a total concentration T of buffer molecules for each species,
%   an acid dissociation constant K for each species, and a starting pH,
%   all taken from the input model,
%   set the initial concentration of conjugate base molecules [B^-]
%   for the reaction HB <-> B^- + H^+.
%
%   From the equilibrium equation: K = [B][H]/[HB] = [B][H]/(T-[B]),
%   we find the equilibrium value: [B] = K T / (K+[H]).
%
%   Similiarly equilibrate the ammonia/ammonium buffer.
%
%
%     new_model = EQUILIBRATE_BUFFERS(model,'hco3')
%     new_model = EQUILIBRATE_BUFFERS(model,0)
%     new_model = EQUILIBRATE_BUFFERS(model,<anything not false>)
%   
%   If the second argument is supplied, and not false, also
%   equilibrate the bicarbonate reaction.  If the second argument is
%   'hco3', modifify [HCO_3^-]_c. If the second argument is anything
%   else not false (e.g.: not 0 or []), then instead equilibrate the
%   bicarbonate equation by adjusting [CO_2].

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = equilibrate_buffers(model,bicarb)
    % intrinsic pit buffers
    H_p = 10^-model.ph_p;
    K_p = (model.Boff_p ./ model.Bon_p);
    model.B_p = model.TB_p .* K_p ./ (K_p + H_p);

    % intrinsic cytoplasm buffers
    H_c = 10^-model.ph_c;
    K_c = (model.Boff_c ./ model.Bon_c);
    model.B_c = model.TB_c .* K_c ./ (K_c + H_c);

    % cytoplasm ammonia buffer
    N = model.nh3_c + model.nh4_c;
    % K = [NH3] * [H] / [NH4+] = [NH3] * [H] / (N-[NH3])
    % [NH3] = K N / (K + [H])
    K = model.nh3_koff / model.nh3_kon;
    model.nh3_c = K * N / (K + H_c);
    model.nh4_c = N - model.nh3_c;

    % force bicarb to equilibrium either by modifying
    % the co2 concentration or the bicarb concentration
    if nargin == 2
        K = model.kon/model.koff;
        if strcmp(bicarb,'hco3')
            model.hco3_c = K * model.co2 / H_c;
        elseif bicarb
            model.co2 = model.hco3_c * H_c / K;
        end
    end
end
