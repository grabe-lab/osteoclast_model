% PLOT_MODEL_BETA
%    PLOT_MODEL_BETA(model)
%
% Given a model structure, plot the contributions to the
% buffering capacity of the model. Bicarbonate, cytoplasm buffers
% and pit buffers are shown.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function plot_model_beta(model)
    ph = 1:0.01:13;
    h = 10.^-ph;

    beta = zeros(size(ph));
    clf;
    subplot(2,1,1);

    leg = {};
    for i = 1:length(model.Boff_c)
        K = model.Boff_c(i)/model.Bon_c(i);
        T = model.TB_c(i);
        leg{i} = sprintf('B%d',i);
        plot(ph,1000*log(10) * T * K * h ./ (K + h).^2);
        hold on;
        beta = beta + 1000*log(10) * T * K * h ./ (K + h).^2;
    end

    plot(ph,beta,'--');
    leg{length(leg)+1} = 'intrinsic';

    ymax = ceil(max(beta)/10)*10 + 40;

    K = model.kon / model.koff;
    hco3 = K * model.co2 ./ h;
    plot(ph,1000*log(10)*hco3,'--');
    beta = beta + 1000*log(10)*hco3;
    leg{length(leg)+1} = 'bicarb';

    plot(ph,beta,'-.')
    leg{length(leg)+1} = 'total';
    
    axis([1 13 0 ymax]);

    legend(leg);
    xlabel('pH');
    ylabel('\beta [mM/pH]');
    title('cytoplasm buffers');
    set(gcf,'color','w');
    set(gca,'fontsize',14);

    subplot(2,1,2);

    leg = {};

    beta = zeros(size(ph));
    
    for i = 1:length(model.Boff_p)
        K = model.Boff_p(i)/model.Bon_p(i);
        T = model.TB_p(i);
        leg{i} = sprintf('B%d',i);
        plot(ph,1000*log(10) * T * K * h ./ (K + h).^2);
        hold on;
        beta = beta + 1000*log(10) * T * K * h ./ (K + h).^2;
    end

    plot(ph,beta,'-.')
    leg{length(leg)+1} = 'total';

    ymax = ceil(max(beta)/10)*10 + 40;
    axis([1 13 0 ymax]);

    xlabel('pH');
    ylabel('\beta [mM/pH]');
    title('pit buffers');
    set(gcf,'color','w');
    set(gca,'fontsize',14);
    legend(leg);
end
