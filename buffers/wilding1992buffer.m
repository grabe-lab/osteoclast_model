% Replace the current cytoplasmic intrinsic buffer with
% the cytoplasmic buffer of rat osteoclasts from:
%   Wilding, T. J., B. Cheng, and A. Roos.
%   pH regulation in adult rat carotid body glomus cells.
%   Importance of extracellular pH, sodium and potassium.
%   Journal ofGeneralPhysiology. 1992 100:593-608.
%
% See figure 8.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = wilding1992buffer(model)
    model.TB_c = 0.041; % [M]
    model.Bon_c = 1e8;  % [M^-1 s^-1] arbitrary, but pretty fast
    pK = 6.41;
    K = 10.^-pK;
    H = 10.^-model.ph_c;
    model.Boff_c = K .* model.Bon_c;
    model.B_c = model.TB_c .* K ./ (K + H);
end
