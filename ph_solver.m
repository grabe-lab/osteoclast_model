% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%
function dxdt=ph_solver(t, x, p, pumpF)

physical_constants;
c2n_c = NA * p.V_c * 1000;  % concentration in [M] to number in the cytoplasm
c2n_p = NA * p.V_p * 1000;  % concentration in [M] to number in the pit

npit = 5 + length(p.TB_p);
n_h_p    = x(1);
n_k_p    = x(2);
n_na_p   = x(3);
n_cl_p   = x(4);
n_D_p    = x(5);  % non-buffering Donnan particles
n_B_p    = x(6:npit);
n_h_c    = x(npit+1);
n_k_c    = x(npit+2);
n_na_c   = x(npit+3);
n_nh4_c  = x(npit+4);
n_nh3_c  = x(npit+5);
n_cl_c   = x(npit+6);
n_hco3_c = x(npit+7);
n_D_c    = x(npit+8);
n_B_c    = x(npit+9:end);

h_p    = n_h_p   /c2n_p;  % [M]
k_p    = n_k_p   /c2n_p;
na_p   = n_na_p  /c2n_p;
cl_p   = n_cl_p  /c2n_p;
D_p    = n_D_p   /c2n_p;
B_p    = n_B_p   /c2n_p;
h_c    = n_h_c   /c2n_c;
k_c    = n_k_c   /c2n_c;
na_c   = n_na_c  /c2n_c;
nh4_c  = n_nh4_c /c2n_c;
nh3_c  = n_nh3_c /c2n_c;
cl_c   = n_cl_c  /c2n_c;
hco3_c = n_hco3_c/c2n_c;
D_c    = n_D_c   /c2n_c;
B_c    = n_B_c   /c2n_c;

ph_p  = -log10(h_p);
ph_c  = -log10(h_c);
h_e = 10^(-p.ph_e);    % extracellular free proton concentration [M]

if any([x(1:4); x(npit+1:npit+7); x(npit+9:end)] < 0) || ~isreal(x)
    if p.verbose
        print_params(t,x,p);
    end
    error('Non-physical values!');
end

u_p = reduced_potential(n_h_p+n_k_p+n_na_p-n_cl_p-n_D_p-sum(n_B_p), p.C0, p.S_p);
u_c = reduced_potential(n_h_p+n_k_p+n_na_p-n_cl_p-n_D_p-sum(n_B_p)+...
                        n_h_c+n_k_c+n_na_c-n_cl_c-n_D_c-sum(n_B_c)+...
                        n_nh4_c-n_hco3_c, p.C0, p.S_c);

% see which currents are on at this time
hleak_p_on   = t>=p.t_hleak_p;
clleak_p_on  = t>=p.t_clleak_p;
clleak_e_on  = t>=p.t_clleak_e;
kleak_p_on   = t>=p.t_kleak_p;
kleak_e_on   = t>=p.t_kleak_e;
naleak_p_on  = t>=p.t_naleak_p;
naleak_e_on  = t>=p.t_naleak_e;
nh3leak_e_on = t>=p.t_nh3leak_e;
hco3_on      = t>=p.t_hco3;
hv1_on       = t>=p.t_hv1;
clc_on       = t>=p.t_clc;
ae2_on       = t>=p.t_ae2;
nhe_on       = t>=p.t_nhe;
vatp_on      = t>=p.t_vatp;
nak_on       = t>=p.t_nak;
hv1_e_on     = t>=p.t_hv1_e;
B_p_on       = t>=p.t_B_p;
B_c_on       = t>=p.t_B_c;
nh3_c_on     = t>=p.t_nh3_c;
nh3_e_on     = t>=p.t_nh3_e;

% see which additional fluxes are being injected into the system
fh_p    = inject(p.fh_p   ,t);
fcl_p   = inject(p.fcl_p  ,t);
fk_p    = inject(p.fk_p   ,t);
fna_p   = inject(p.fna_p  ,t);
fh_c    = inject(p.fh_c   ,t);
fcl_c   = inject(p.fcl_c  ,t);
fk_c    = inject(p.fk_c   ,t);
fna_c   = inject(p.fna_c  ,t);
fhco3_c = inject(p.fhco3_c,t);
fD_p    = inject(p.fD_p   ,t);
fD_c    = inject(p.fD_c   ,t);
fnh3_e  = inject(p.fnh3_e ,t);

nh3_e = fnh3_e;  % this is a hack to kill the external nh3 for nordstrom
                 % instead of a flux, it changes the total concentration
                 % we don't use the flux, like above, because nh3_e(t) is
                 % not a tracked dependent variable.
if nh3_e < 1e-9
    nh3_e = 1e-9; % prevent negative nh3_c concentrations
end

% flux for the V-ATPase
% interpolate the current state to find proton pumping value
if vatp_on
%    J_v = interp3(pumppHp, pumpV, pumppHc, pumpz(:,:,:,4), ...
%                  ph_p, u_p*kTe, ph_c, 'spline');
    J_v = pumpF(-ph_p, u_p*kTe, ph_c);
else
    J_v = 0;
end

% fluxes into the pit from the cytoplasm
dh_vatp_p  = vatp_on     * (  p.N_v    * J_v);
dh_hv1_p   = hv1_on      * (- p.N_hv1  * J_hv1(h_p, h_c, u_p, NA, kTe));
dcl_clc_p  = clc_on      * (2*p.N_clc  * J_clc(cl_p,u_p,cl_c,ph_c,ph_p));
dh_leak_p  = hleak_p_on  * (- p.P_h_p  * p.S_p * J_leak(h_p, h_c,  u_p, NA));
dcl_leak_p = clleak_p_on * (- p.P_cl_p * p.S_p * J_leak(cl_p, cl_c, -u_p, NA));
dk_leak_p  = kleak_p_on  * (- p.P_k_p  * p.S_p * J_leak(k_p,  k_c,   u_p, NA));
dna_leak_p = naleak_p_on * (- p.P_na_p * p.S_p * J_leak(na_p, na_c,  u_p, NA));

% fluxes to the extracellular region from the cytoplasm
dcl_ae2_e  = ae2_on      * (  p.N_ae2   * J_ae2(cl_c, p.cl_e, hco3_c, p.hco3_e, ph_c));
dna_nhe_e  = nhe_on      * (  p.N_nhe   * J_nhe(na_c, p.na_e, ph_c, p.ph_e));
dk_leak_e  = kleak_e_on  * (- p.P_k_c   * p.S_c * J_leak(p.k_e,  k_c,  -u_c, NA));
dcl_leak_e = clleak_e_on * (- p.P_cl_c  * p.S_c * J_leak(p.cl_e, cl_c,  u_c, NA));
dna_leak_e = naleak_e_on * (- p.P_na_c  * p.S_c * J_leak(p.na_e, na_c, -u_c, NA));
dnh3_leak_e= nh3leak_e_on* (- p.P_nh3_c * p.S_c * J_leak(nh3_e,nh3_c,   0, NA));
dna_nak_e  = nak_on      * (3*p.N_nak   * J_nak_steady(c2n_c));
dh_hv1_e   = hv1_e_on    * (- p.N_hv1_e * J_hv1(h_e, h_c, -u_c, NA, kTe));

% special case for teti fig 6:  external cl is washed out in step 2 of 3.
% we want to see what happens if ae2 is not allowed to respond to this cl_e change.
% the rest of the system will respond (ex: dcl_leak_e
if isfield(p,'ae2_cl_e')
    dcl_ae2_e = ae2_on * (p.N_ae2 * J_ae2(cl_c, p.ae2_cl_e, hco3_c, p.hco3_e, ph_c));
end

% cytoplasmic H20 + CO2 <-> H2CO3 <-> H+ + HCO3-
dhco3_cdt = hco3_on * ((p.kon*p.co2 - p.koff*h_c*hco3_c)*c2n_c);
%
% Note the on/off notational difference between bicarb and other buffers
% is because the on/off constants for bicarb are for
% association/dissociation of H2CO3 from/to H20 and C02,
% not for H2CO3 <-> H+ + HCO3-.

% other buffers
dnh4_cdt = nh3_c_on * (p.nh3_kon * nh3_c * h_c - p.nh3_koff * nh4_c) * c2n_c;
if n_nh4_c + dnh4_cdt < 0
    dnh4_cdt = - nh3_c_on * n_nh4_c;
end

dB_pdt = B_p_on * (p.Boff_p .* (p.TB_p - B_p) - p.Bon_p .* B_p * h_p) * c2n_p;
dB_cdt = B_c_on * (p.Boff_c .* (p.TB_c - B_c) - p.Bon_c .* B_c * h_c) * c2n_c;

% note the 1 H+ per 2 Cl- in dh_pdt
dh_pdt = dh_vatp_p + dh_hv1_p - dcl_clc_p/2 + dh_leak_p + sum(dB_pdt);
dh_cdt = - dh_pdt + dhco3_cdt + dna_nhe_e - dh_hv1_e - dnh4_cdt + sum(dB_cdt);

% ODE: x = [h_p,cl_p,k_p,na_p,h_c,cl_c,k_c,na_c,hco3_c,ph_p,ph_c];
% dxdt = injected fluxes + transporter fluxes + buffer fluxes
dxdt=[fh_p    + dh_pdt;
      fk_p    + dk_leak_p;
      fna_p   + dna_leak_p;
      fcl_p   + dcl_clc_p + dcl_leak_p;
      fD_p;
                dB_pdt;
      fh_c    + dh_cdt;
      fk_c    - dk_leak_p - dk_leak_e + 2/3 * dna_nak_e;  % +2K/-3Na for NaK
      fna_c   - dna_leak_p - dna_nhe_e - dna_nak_e;
              + dnh4_cdt;
              - dnh4_cdt - dnh3_leak_e;
      fcl_c   - dcl_clc_p - dcl_leak_p - dcl_ae2_e - dcl_leak_e;
      fhco3_c + dhco3_cdt + dcl_ae2_e;
      fD_c;
                dB_cdt;
      ];

% p.return_currents should not be set while running the ode solver.
if p.return_currents
    dxdt = [dh_vatp_p;
            dh_hv1_p;
            dcl_clc_p;
            dh_leak_p;
            dcl_leak_p;
            dk_leak_p;
            dna_leak_p;
            dcl_ae2_e;
            dna_nhe_e;
            dk_leak_e;
            dcl_leak_e;
            dna_leak_e;
            dna_nak_e;
            dh_hv1_e;
            dhco3_cdt;
            dnh3_leak_e;
            dnh4_cdt;
            dB_pdt;
            dB_cdt;
            fh_p;
            fk_p;
            fna_p;
            fcl_p;
            fD_p;
            fh_c;
            fk_c;
            fna_c;
            fcl_c;
            fhco3_c;
            fD_c];

end
end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% ClC-7 flux
function y = J_clc(cl_p, u, cl_c, ph_c, ph_p)

    if abs(u) > 2.5
        warning('operating outside of experimental range.');
    end

    a = 7.7070;
    b = 0.2543;

    if u < -

    mu = 3*u+2*log(cl_c/cl_p)+2.3*(ph_c-ph_p);
    x = 0.5+0.5*tanh((mu+9.7314)/2.9194);
    y = x*a*mu+(1.0-x)*b*(mu)^3;
end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% chloride bicarbonate antiporter
% Leem et al. 1999 style ph dependence added to ae2 current
function y = J_ae2(cl_c, cl_e, hco3_c, hco3_e, ph_c)

    V_max=1.454e3;  % find better values
    Kd=1;         % find better values
    A = 6.7; % this is the Leem value
    if ph_c < A
        P = 0;
    elseif ph_c >= 7.45
        warning('J_ae2 is not well modeled above pH 7.45 due to lack of experimental data.');
        P = 1;
    else
        P = (ph_c-A)/(7.45-A);
    end
    P = P^2;        % soften Leem et al. (1999)'s linear pH dependence

    mu = log(cl_c/cl_e)+log(hco3_e/hco3_c);
    y = P*V_max*mu/(Kd+abs(mu));

end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% sodium proton antiporter
function y = J_nhe(na_c, na_e, ph_c, ph_e)

    V_max=1.454e3;  % find better values
    Kd=3.0;         % find better values

    if ph_c > 7.25
        P = 0;
    elseif ph_c <= 6.55
        warning('J_nhe is not well modeled below pH 6.55 due to lack of experimental data.');
        P = 1;
    else
        P = ((ph_c-7.25)/(6.55-7.25)).^2;
    end

    mu = log(na_c/na_e)+2.3*(ph_c-ph_e);
    y = P*V_max*mu/(Kd+abs(mu));

end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% Steady state effect of Na+/K+-ATPase on cytoplasmic Na+.
% Positive current of Na+ means cytoplasmic to extracellular.
function y = J_nak_steady(c2n_c)
    % 0.15 mM/min * 1min/60s * 1e-3 M/mM = 2.5e-6 M/s
    % divide by 3 to get the number of turnovers per second
    y = 2.5e-6 * c2n_c / 3;
end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% leak current across membrane for Cl/H/K
function y = J_leak(a, b, u, NA)
    limit = 1e-6;
    if abs(u) > limit
        y = u*(a-b*exp(-u))/(1-exp(-u));
    elseif u == 0
        y = (a-b);
    else
        y = (b*exp(-u)-a)*(u/expm1(-u));
    end
    y = y*1e-3*NA; % 10^-3 [liter to cm^3]; NA for #; y = [#/cm^3]
end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
% Hv1 model across the ruffled border
% Positive current is protons leaving the pit
function y = J_hv1(hf_p, hf_c, u, NA, kTe)
    g = 8.5805e+08; % scale factor from current fits [#/M/s]
    limit = 1e-6;

    if abs(u) > limit
        y = u*(hf_p-hf_c*exp(-u))/(1-exp(-u));
    elseif u == 0
        y = (hf_p-hf_c);
    else
        y = (hf_c*exp(-u)-hf_p)*(u/expm1(-u));
    end
    ph_p = -log10(hf_p);
    ph_c = -log10(hf_c);
    % Since we use the opposite definition of u, the signs of
    % V, V0, and the exponent in P are opposite the usual definition.
    V = u*kTe;                       % membrane potential [mV]
    V_0 = -(-40*(ph_p-ph_c) + 30);   % V_0 = -40*(pH_p-pH_c)+30 [mV]
    P = 1/(1+exp((V-V_0)/8));
    mu = abs(u-log(hf_c/hf_p));      % PMF for moving from cyto to pit
    y = ph_p*g*P*mu*y;               % #/s for a single channel
end
%-------------------------------------------------------------------------%

%-------------------------------------------------------------------------%
function dndt = inject(f,t)
% f.t = [tstart tfinish]
% f.x = [extra arguments to the flux function]
% f.f = function with arguments (t,x)
    dndt = 0;
    if (f.t(1) <= t) && (t < f.t(2))
        dndt = f.f(t,f.x);
    end
end
%-------------------------------------------------------------------------%
