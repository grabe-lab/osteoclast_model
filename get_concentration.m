% The existence of this function indicates that we have a bad
% abstraction for our data (a 2d array).
%
% If you change the order or contents of the dependent variables
% in the solver, you have to change this function.
%
% Either model or npit is needed to determine how many pit
% buffer species there are.
% npit is the total number of pit ion species, including
% buffers and Donnan particles.
%
% So use either:
%    k_c = get_concentration('k_c',y,model)
% or:
%    k_c = get_concentration('k_c',y,0,number_of_pit_species)
%
% In the former case, the number of pit species is determined from the model.
% In the later case, the model is ignored, and the input number is used.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function conc = get_concentration(name,x,model,npit)

    names_p = {'h_p';'k_p';'na_p';'cl_p';'D_p';'B_p'};
    names_c = {'h_c';'k_c';'na_c';'nh4_c';'nh3_c';...
               'cl_c';'hco3_c';'D_c';'B_c'};
    
    if nargin > 3
        nB_p = npit - length(names_p) + 1;
    else
        nB_p = length(model(1).TB_p);
        npit = length(names_p) - 1 + nB_p;
    end

    ind = strmatch(name,names_p);
    if length(ind) == 1
        if ind == length(names_p)
            conc = x(:,ind:npit);
        else
            conc = x(:,ind);
        end
        return
    end

    ind = strmatch(name,names_c);
    if length(ind) == 1
        if ind == length(names_c)
            conc = x(:,npit+ind:end);
        else
            conc = x(:,npit+ind);
        end
        return
    end

    error('species %s unknown', name);
end
