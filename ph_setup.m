% Usage:
%   ph_setup():     Run the solver using the default parameters
%                   from the model structure in model_parameters.m.
%
%   ph_setup(fit,model):
%                   Replace some model parameters with fit parameters.
%                   model.fit must be a cell array of field names.
%                   if model.fit(k) == 'foo', then model.foo is set to fit(k).
%
%                   If model.fit doesn't exist, fit is ignored.
%
%   ph_setup(fit,models):
%                   Run the solver in a loop for each model in models.
%                   Run using models(k) from time models(k-1).tfinal to
%                   models(k).tstart.
%                   Time dependent variables returned by the solver for
%                   models(i) will override time dependent variables in
%                   models(i+1).  I.e., The solver output ph_p for model i
%                   overwrite the models(i+1).ph_p for the next solver call.
%
%   ph_setup(fit,models,x0):
%                   The first time the solver is started, use x = x0,
%                   instead of setting x from models. (x is the set of
%                   dependent variables.  This allows you to restart 
%                   the model at the final values of a previous run.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README

function [t, y, pot_p, pot_c, dx] = ph_setup(fit, models, x0)

%%%%%%%%%%%% input parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2
    % if model parameters are not specified, use the default values.
    models = model_parameters();
end

% make sure the model start times are monotonically increasing
times = [models.tstart models(end).tfinal];
assert(all((times(2:end) - times(1:end-1)) > 0),...
       'model start times must be monotonic');

% assign fit parameters to each model in models
if (nargin > 0) && (length(fit) > 0)
    if isfield(models(1),'fit')
        if (length(fit) == length(models(1).fit))
            for m = 1:length(models)
                for k = 1:length(fit)
                    models(m).(models(m).fit{k})(end) = fit(k);
                end
            end
        elseif (length(fit) > 1)
            msg = 'Please set models.fit to a cell array of names of the model parameters cooresponding to the input fit parameters.';
            error(msg);
        end
    end
end

physical_constants;
c2n_p = NA * models(1).V_p * 1000;  % [M] to number in the pit
c2n_c = NA * models(1).V_c * 1000;  % [M] to number in the cytoplasm

if nargin < 3
    p = models(1);

    q_p = enclosed_charge(p.Psi_p/kTe, p.C0, p.S_p); % charge in [#]
    q_c = enclosed_charge(p.Psi_c/kTe, p.C0, p.S_c);

    h_p = 10^-p.ph_p;
    h_c = 10^-p.ph_c;

    % set the concentration of impermeant ions to get the initial potentials
    D_p =       -q_p /c2n_p + (h_p + p.k_p + p.na_p           - p.cl_p - sum(p.B_p));
    D_c = - (q_c-q_p)/c2n_c + (h_c + p.k_c + p.na_c + p.nh4_c - p.cl_c - sum(p.B_c) - p.hco3_c);

    % x0 is in [M] so that the input parameter x0 can also be in [M]
    x0 = [h_p; p.k_p; p.na_p;                   p.cl_p;           D_p; p.B_p;
          h_c; p.k_c; p.na_c; p.nh4_c; p.nh3_c; p.cl_c; p.hco3_c; D_c; p.B_c];
end

% set initial conditions of time dependent variables.
% x0 is in concentration, but x is in numbers.
x = x0;
npit = 5 + length(p.B_p);
x(    1:npit) = x(    1:npit) * c2n_p;
x(npit+1:end) = x(npit+1:end) * c2n_c;

%%%%%%%%%%%% fixed parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% V-ATPase pumping surface
% v_0001 was used in: Grabe and Oster (2001) J. Gen. Physiol. 117:329-343 
%                     Ishida et al. (2013) J. Gen. Physiol. 141: 705-720
% created in: /Users/mgrabe/Projects/pH Regulation/Grabe_Oster_2001_model/V-ATPase_code
% created in: /Users/mgrabe/Projects/pH Regulation/Ishida_2013_model/V-ATPase_code
% v_0002 has a 10 H+ : 3 ATP stoichiometry and a few minor changes from v_0001
% created in: /Users/mgrabe/Projects/pH Regulation/Marcoline_2016_model/V-ATPase_code
% v_XXXX(psi, pH pit, pH cyto, n)
% v_0003 is like v_0002, but lacks the effective pH enhancement at the VATPase surface
if isfield(p,'pumpz')
    mfile = p.pumpz;
else
    mfile = 'v_0003';
end
mat = matfile(mfile);
pumpz = mat.(mfile);        % proton flux per VATPase
pumpV = -300:20:300;        % psi
pumppHp = 9:-0.2:0;         % pit pH
pumppHc = 6:0.2:14;         % cytoplasm pH

pumpF = griddedInterpolant({-pumppHp,pumpV,pumppHc}, ...
                           permute(pumpz(:,:,:,4),[2,1,3]), 'spline');
%%%%%%%%%%%% run solver for each input model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t = [];
y = [];
dx = [];
pot_p = [];
pot_c = [];
for m = 1:length(models)
    p = models(m);
    p.return_currents = false;  % this must be false to solve the ODE

                                % options of ode solver
    options = odeset('RelTol',p.reltol,'AbsTol',p.abstol);

    c2n_c = NA * p.V_c * 1000;  % concentration [M] to number in the cytoplasm
    c2n_p = NA * p.V_p * 1000;  % concentration [M] to number in the pit

    % Each time a current is turned on stop and restart the solver,
    % so that the solver doesn't step over the turn-on time.
    if m < length(models)
        tstop = models(m+1).tstart;
    else
        tstop = p.tfinal;
    end
    on = unique([p.tstart ...
                 p.t_hleak_p ...
                 p.t_clleak_p p.t_clleak_e ...
                 p.t_kleak_p  p.t_kleak_e ...
                 p.t_naleak_p p.t_naleak_e ...
                 p.t_nh3leak_e ...
                 p.t_nh3_e ...
                 p.t_hco3 p.t_hv1 p.t_clc p.t_ae2 p.t_vatp p.t_nhe ...
                 p.t_hv1_e ...
                 p.fh_p.t p.fcl_p.t p.fk_p.t p.fna_p.t ...
                 p.fh_c.t p.fcl_c.t p.fk_c.t p.fna_c.t ...
                 p.fD_p.t p.fD_c.t p.fhco3_c.t ...
                 tstop], 'sorted');
    on = on((p.tstart <= on) & (on <= tstop));

    % for each model, loop over all channel or flux on/off events.
    for k = 2:length(on)
      % make sure the time range for this step is reasonable
        if ((on(k) - on(k-1)) < 2*p.tstep)
            if p.verbose
                disp(p);
            end
            error(sprintf('The time between two currents turning on (%f and %f) in step %d of model %d is too small compared to time step (%f).',...
                          on(k-1),on(k),k,m,p.tstep));
        end

        if p.verbose
            fprintf('Ode15s is running model %d of %d, step %d of %d...',...
                    m,length(models),k-1,length(on)-1);
        end

        try
            [odet,odey] = ode15s(@ph_solver,(on(k-1):p.tstep:on(k)),x,...
                                 options,p,pumpF);

            % if the start point coincides exactly with the previous end point,
            % don't duplicate that point.
            if (length(t) > 0) && (t(end) == odet(1))
                t = t(1:end-1);
                y = y(1:end-1,:);
                pot_p = pot_p(1:end-1);
                pot_c = pot_c(1:end-1);
                if length(dx) > 0
                    dx = dx(1:end-1,:);
                end
            end

            x = odey(end,:);
            t = [t; odet];
            y = [y; [odey(:,1:npit)/c2n_p odey(:,npit+1:end)/c2n_c]];
            q_p = sum(odey(:,       (1:3)),2) - sum(odey(:,     4:npit),2);
            q_c = sum(odey(:,npit + (1:4)),2) - sum(odey(:,npit+6:end), 2);
            pot_p = [pot_p; reduced_potential(q_p,       p.C0, p.S_p)*kTe];
            pot_c = [pot_c; reduced_potential(q_p + q_c, p.C0, p.S_c)*kTe];

        catch err
            % We have failed, probably due to bad input parameters.
            % Let's return a set of Infs so that amoeba can continue to run,
            % and give this a bad score.
            if p.verbose
                fprintf('file %s, function %s, line %d:\n',err.stack(1).file,err.stack(1).name,err.stack(1).line);
                disp(err.message);
            end
            if length(y) > 0
                y(end,:) = Inf;
            else
                y = Inf(size(x));
            end
            return;
        end

        if p.currents
            p.return_currents = true;  % tell the solver to return currents
            if p.verbose
                fprintf('Recording currents...');
            end

            nt = length(odet);
            dxm1 = ph_solver(odet(1),transpose(odey(1,:)),p,pumpF);
            dxm = zeros(nt,length(dxm1));
            dxm(1,:) = dxm1;
            for k = 2:nt
                dxm(k,:) = ph_solver(odet(k),transpose(odey(k,:)),p,pumpF);
            end
            dx = [dx; dxm];
            p.return_currents = false;
        end

        if p.verbose
            fprintf('Done.\n')
        end
    end
end

if p.plot
    if ~ischar(p.plot)
        p.plot='-';
    end
    make_plots(t,y,pot_p,pot_c,npit,dx,p.plot);
end

end
