% make_plots
%     This function is called by ph_setup(params,model) if model.plot == true.
%     To call it manually after a run:
%         [t,x,pot_p,pot_c,dx] = ph_setup(params,model);
%         make_plots(t,x,pot_p,pot_c,dx,'--');
%     The final arguement is a plot linestyle (one of '-','--','-.', and ':')
%     and defaults to '-' if not provided.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
function make_plots(t,x,pot_p,pot_c,npit,dx,style)
    if (nargin < 7) || ~ischar(style)
        style = '-';
    end

    plot_variables(t,x,pot_p,pot_c,npit,style);

    if (nargin > 6) && (length(dx) > 1)
        plot_fluxes(t,dx,npit,style);
    end
end


function plot_variables(t,x,pot_p,pot_c,npit,style)
    h_p    = get_concentration('h_p'   ,x,[],npit);
    k_p    = get_concentration('k_p'   ,x,[],npit);
    na_p   = get_concentration('na_p'  ,x,[],npit);
    cl_p   = get_concentration('cl_p'  ,x,[],npit);
    D_p    = get_concentration('D_p'   ,x,[],npit);
    B_p    = get_concentration('B_p'   ,x,[],npit);
    h_c    = get_concentration('h_c'   ,x,[],npit);
    k_c    = get_concentration('k_c'   ,x,[],npit);
    na_c   = get_concentration('na_c'  ,x,[],npit);
    nh4_c  = get_concentration('nh4_c' ,x,[],npit);
    nh3_c  = get_concentration('nh3_c' ,x,[],npit);
    cl_c   = get_concentration('cl_c'  ,x,[],npit);
    hco3_c = get_concentration('hco3_c',x,[],npit);
    D_p    = get_concentration('D_p'   ,x,[],npit);
    B_p    = get_concentration('B_p'   ,x,[],npit);

    ph_p   = -log10(h_p);
    ph_c   = -log10(h_c);

    nf = 4;

    if min(nh3_c) ~= max(nh3_c)
        show_nh3 = true;
        nf = nf + 1;
    else
        show_nh3 = false;
    end
    
    set_figure_by_tag('osteoclast variables');
    
    ns = count_subplots();  % incase one plot modifies nh3, but the subsequent doesn't
    if ns > nf
        nf = ns;
    end

    subplot(nf,1,1);
    hold on
    plot_pH(t,ph_p,ph_c,style);
    subplot(nf,1,2);
    hold on
    plot_concentrations(t,cl_p,k_p,na_p,[],style);
    subplot(nf,1,3);
    hold on
    plot_concentrations(t,cl_c,k_c,na_c,hco3_c,style);
    subplot(nf,1,4);
    hold on
    plot_potential(t,pot_p,pot_c,style);

    if show_nh3
        subplot(nf,1,5);
        hold on;
        plot_nh3(t,nh4_c,nh3_c,style);
    end
    xlabel('time [s]');
end
function plot_pH(t,ph_p,ph_c,style)
    cv = ['b' 'r'];
    hold on
    plot(t,ph_p,'LineStyle',style,'Color',cv(1));
    plot(t,ph_c,'LineStyle',style,'Color',cv(2));
    ylabel('pH');
    legend('pit','cyto');
    box on;

    %axes('Position',[0.46 0.8 0.33 0.08]);
    %plot(t,ph_c,'r');
    %ax=axis();

    %axis([ax(1:2) min(ph_c)-0.01 max(ph_c)+0.01]);
    %box off;
    %xlabel('time [s]');
    %ylabel('pH');
    %set(gca,'FontSize',8);
end
function plot_concentrations(t,cl,k,na,hco3,style)
    if length(hco3) == 0
        c = 'in pit';
    else
        c = 'in cyto';
    end
    cv = ['g','b','y','c','r','k','m'];
    hold on
    plot(t,cl,  'LineStyle',style,'Color',cv(1));
    plot(t,k,   'LineStyle',style,'Color',cv(5));
    plot(t,na,  'LineStyle',style,'Color',cv(2));
    leg={'Cl^-','K^+','Na^+'};
    if length(hco3) > 0
        plot(t,hco3,'LineStyle',style,'Color',cv(7));
        leg{4} = 'HCO_3^-';
    end
    ylabel({'concentration [M]',c});
    legend(leg);
    box on;
end
function plot_potential(t,pot_p,pot_c,style)
    cv = ['b' 'r'];
    hold on
    plot(t,-pot_p,'LineStyle',style,'Color',cv(1));
    plot(t, pot_c,'LineStyle',style,'Color',cv(2));
    ylabel('membrane potential [mV]')
    legend('cytoplasm-pit','cytoplasm-extracellular');
    box on;
end

function plot_nh3(t,nh4,nh3,style)
    cv = ['y','g'];
    plot(t,nh4,'LineStyle',style,'Color',cv(1));
    plot(t,nh3,'LineStyle',style,'Color',cv(2));
    ylabel('cyto NH_3 and NH_4^+ [M]');
    legend('NH_4^+','NH_3');
    box on;
end

function plot_fluxes(t,dx,npit,style)
    dh_vatp_p  = dx(:,1);
    dh_hv1_p   = dx(:,2);
    dcl_clc_p  = dx(:,3);
    dh_leak_p  = dx(:,4);
    dcl_leak_p = dx(:,5);
    dk_leak_p  = dx(:,6);
    dna_leak_p = dx(:,7);
    dcl_ae2_e  = dx(:,8);
    dna_nhe_e  = dx(:,9);
    dk_leak_e  = dx(:,10);
    dcl_leak_e = dx(:,11);
    dna_leak_e = dx(:,12);
    dna_nak_e  = dx(:,13);
    dh_hv1_e   = dx(:,14);
    dhco3_cdt  = dx(:,15);
    dnh3_leak_e= dx(:,16);
    dnh4_cdt   = dx(:,17);

    pit_on = false;
    for k = 1:7
        mi = min(dx(:,k));
        ma = max(dx(:,k));
        if ma-mi > 1e-8
            pit_on = true;
            break;
        end
    end

    nh3_on = false;
    if nnz(dnh4_cdt) > 0 || nnz(dnh3_leak_e) > 0
        nh3_on = true;
    end

    nd = 17; % fixed terms
    nf = 11; % explicit flux terms

    nBp        = npit - 5;
    dB_pdt     = dx(:,nd+1:nd+nBp);
    dB_cdt     = dx(:,nd+nBp+1:end-nf);

    df         = dx(:,end-nf+1:end);
    plot_df    = any(df(:));

    set_figure_by_tag('osteoclast fluxes');


    % if there are already plots in this window,
    % use this window's number of subplots
    ns = count_subplots();
    if ns == 0
        ns = 3 + 2 * pit_on + nh3_on + plot_df;
    end


    s = 1;
    if pit_on
        subplot(ns,1,s);
        s = s + 1;
        hold on;
        plot_pit(t,dh_vatp_p,dh_hv1_p,dcl_clc_p,dh_leak_p,dcl_leak_p,dk_leak_p,dna_leak_p,style);
        box on;
    end
    subplot(ns,1,s);
    s = s + 1;
    hold on;
    plot_cyto(t,dcl_ae2_e,dna_nhe_e,dk_leak_e,dcl_leak_e,dna_leak_e,dna_nak_e,dh_hv1_e,style);
    box on;
    if pit_on
        subplot(ns,1,s);
        s = s + 1;
        hold on;;
        plot_buffers(t,dB_pdt,'pit',style);
        box on;
    end
    subplot(ns,1,s);
    s = s + 1;
    hold on;;
    plot_buffers(t,dB_cdt,'cyto',style);
    box on;
    subplot(ns,1,s);
    s = s + 1;
    hold on;
    plot_bicarb(t,dhco3_cdt,style);
    box on;
    if nh3_on
        subplot(ns,1,s);
        s = s + 1;
        hold on;
        plot_dnh3(t,dnh3_leak_e,dnh4_cdt,style);
        box on;
    end
    if plot_df
        subplot(ns,1,s);
        s = s + 1;
        hold on;
        plot_explicit_fluxes(t,df,style);
        box on;
    end
    xlabel('time [s]');
end

function plot_explicit_fluxes(t,df,style)
    labels = {'H^+_p'; 'K^+_p'; 'Na^+_p'; 'Cl^-_p'; 'D^-_p';
              'H^+_c'; 'K^+_c'; 'Na^+_c'; 'Cl^-_c'; '{HCO_3}^-_c'; 'D^-_c'};
    n = length(labels);
    if size(df,2) ~= n
        error('wrong number of flux labels');
    end

    leg = {};
    count = 0;
    for k = 1:n
        if nnz(df(:,k))
            count = count + 1;
            leg{count} = labels{k};
            plot(t, df(:,k),'LineStyle',style);
            hold on;
        end
    end
    if count > 0
        ylabel('Explicit fluxes [#/s]');
        xlabel('');
        legend(leg);
        set(gca,'fontsize',12);
    end
end


function plot_pit(t,dh_vatp_p,dh_hv1_p,dcl_clc_p,dh_leak_p,dcl_leak_p,dk_leak_p,dna_leak_p,style)
    cv = ['g','b','y','c','r','k','m'];
    plot(t, dh_vatp_p,  'Color',cv(7),'LineStyle',style);
    plot(t, dh_hv1_p,   'Color',cv(2),'LineStyle',style);
    plot(t, dh_leak_p,  'Color',cv(4),'LineStyle',style);
    plot(t,-dcl_clc_p/2,'Color',cv(3),'LineStyle',style);
    plot(t, dcl_clc_p,  'Color',cv(1),'LineStyle',style);
    plot(t, dcl_leak_p, 'Color',cv(6),'LineStyle',style);
    plot(t, dk_leak_p,  'Color',cv(5),'LineStyle',style);
    ylabel('flux into pit [#/s]');
    legend('V-ATP H^+','Hv1   H^+','leak  H^+','ClC  H+','ClC   Cl^-','leak  Cl^-','leak  K^+');
    set_axis(t,dh_vatp_p,dh_hv1_p,dcl_clc_p,dh_leak_p,-dcl_clc_p/2,dcl_leak_p,dk_leak_p,dna_leak_p)
    set(gca,'FontSize',12);
end


function plot_cyto(t,dcl_ae2_e,dna_nhe_e,dk_leak_e,dcl_leak_e,dna_leak_e,dna_nak_e,dh_hv1_e,style)
    cv = ['g','b','y','c','r','k','m'];
    hold on;
    plot(t,-dcl_ae2_e, 'Color',cv(1),'LineStyle',style);
    plot(t,-dna_nhe_e, 'Color',cv(2),'LineStyle',style);
    plot(t,-dk_leak_e, 'Color',cv(5),'LineStyle',style);
    plot(t,-dcl_leak_e,'Color',cv(3),'LineStyle',style);
    plot(t,-dna_nak_e, 'Color',cv(7),'LineStyle',style);
    plot(t,-dh_hv1_e,  'Color',cv(4),'LineStyle',style);
    ylabel('ec to cyto flux [#/s]');
    legend('AE2 Cl^-','NHE Na^+','leak K^+','leak Cl^-','Na^+/K^+ Na^+','Hv1 H^+');
    set_axis(t,-dcl_ae2_e,-dna_nhe_e,-dk_leak_e,-dcl_leak_e,-dna_nak_e,-dh_hv1_e);
    set(gca,'FontSize',12);

end

function plot_bicarb(t,dhco3_cdt,style)
    cv = ['g','b','y','c','r','k','m'];
    plot(t,dhco3_cdt,  'Color',cv(7),'LineStyle',style);
    ylabel({'cyto','H_2CO_3 -> HCO_3^- + H^+','[#/s]'});
    set_axis(t,dhco3_cdt);
    set(gca,'FontSize',12);

end

function plot_dnh3(t,dnh3_leak_e,dnh4_cdt,style)
    cv = ['g','b','y','c','r','k','m'];
    plot(t,dnh3_leak_e,'Color',cv(1),'LineStyle',style);
    plot(t,dnh4_cdt,   'Color',cv(3),'LineStyle',style);
    legend('NH_3 leak','NH_3 + H^+ \rightarrow NH_4^+');
    set_axis(t,dnh3_leak_e,dnh4_cdt);
    ylabel('NH_3/NH_4^+');
    set(gca,'FontSize',12);
end

function plot_buffers(t,dB_dt,label,style)
    leg = {};
    co = get(gca,'colororder');
    markers = {'none','x'};
    for k = 1:size(dB_dt,2)
        c = co(1+mod(k-1,size(co,1)),:);
        m = markers{1+(k>length(co))};
        plot(t,dB_dt(:,k),'marker',m,'color',c,'linestyle',style);
        leg{k} = sprintf('B%d',k);
    end
    ylabel({label,'HBn -> Bn^- + H^+','[#/s]'});
    set(gca,'FontSize',12);
    legend(leg);
end

% if we already have a tagged figure, plot over the same figure
function set_figure_by_tag(tag)
    if ~ischar(tag)
        error('use a string tag');
    end

    figs = get(0,'children');  % all figures
    for i = 1:length(figs)
        ud = get(figs(i),'UserData');
        if ischar(ud) && strcmp(tag, ud)
            figure(figs(i));
            return;
        end
    end

    % didn't find the tagged figure, so create a new one, and tag it.
    f = figure();
    set(f,'UserData',tag);
end

function set_axis(x,varargin)
    % Let's let matlab pick the yrange,
    % but don't use the first few values of each variable
    % since we are not interested in the transients.
    % varargin is some non-zero number of plot y variables.

    nskip = 0; % set to the number of points to skip at the start.  10 works

    h = gca;

    % make a temporary plot.
    a = axes();
    hold on;
    for k = 1:(nargin-1)
        y = varargin{k};
        y(1:nskip) = y(end-nskip+1:end);
        plot(x,y);
    end
    ax = axis();
    delete(a);

    % set the axis ranges for the previous plot
    axes(h);
    axis(ax);
end

function nkids = count_subplots()
    kids = get(gcf,'children');
    nkids = 0;
    for k = 1:length(kids)
        if strcmp('axes',get(kids(k),'Type'))
            nkids = nkids + 1;
        end
    end
end
