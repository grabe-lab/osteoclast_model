% PIT_OFF
%   model2 = pit_off(model)
%
% Given a model structure, such as returned by model_parameters(), 
% or array of such structures, turn off all cytoplasm to pit transport,
% and all buffering reactions in the pit.
%
% This is to simulate the cytoplasm in isolation when the resorptive pit
% is not present.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function model = pit_off(mod)

    model = mod;

    pit = {'t_hv1','t_clc','t_vatp','t_hleak_p',...
           't_clleak_p','t_kleak_p','t_naleak_p','t_B_p'};

    for k = 1:length(mod)
        for j = 1:length(pit)
            model(k).(pit{j}) = Inf;
        end
    end

end
