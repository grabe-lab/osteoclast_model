% PRINT_PARAMS
%   Usage:
%     [t,y] = ph_setup(0,p);
%     print_params(t(end),x(end,:),p);
%
% Given an array of values at some time t, print them in a nice readable format.

% Copyright 2014-2016 Regents of the University of California
% See LICENSE and README
%-------------------------------------------------------------------------%

function print_params(t,x,p)
    physical_constants;

    names_p = {'h_p';'k_p';'na_p';'cl_p';'D_p'};
    names_c = {'h_c';'k_c';'na_c';'nh4_c';'nh3_c';'cl_c';'hco3_c';'D_c'};

    npit = length(names_p) + length(p.TB_p);

    % let us do some guess work
    % params could be in numbers or could be in concentration
    ph_p = -log10(x(1));
    if ph_p < 0 || ph_p > 14
        % probably in numbers, not concentration
        c2n_c = NA * p.V_c * 1000;  % M to # in the cytoplasm
        c2n_p = NA * p.V_p * 1000;  % M to # in the pit
        x(1:npit) = x(1:npit) / c2n_p;
        x(npit+1:end) = x(npit+1:end) / c2n_c;
    end

    fprintf('  Time: %10.4e  s\n',t);
    fprintf('%6s: %10.3e  M\n',names_p{1},x(1));
    for k = 2:length(names_p)
        fprintf('%6s: %10.4f mM\n',names_p{k},1000*x(k));
    end
    for k = length(names_p)+1:npit
        fprintf('  B%d_p: %10.4f mM\n',k-length(names_p),1000*x(k));
    end
    fprintf('%6s: %10.3e  M\n',names_c{1},1000*x(npit+1));
    for k = npit+2:npit+length(names_c)
        fprintf('%6s: %10.4f mM\n',names_c{k-npit},1000*x(k));
    end
    for k = npit+length(names_c)+1:length(x)
        fprintf('  B%d_c: %10.4f mM\n',k-npit-length(names_c),1000*x(k));
    end
    fprintf('  pH_p: %10.4f\n',-log10(x(1)));
    fprintf('  pH_c: %10.4f\n',-log10(x(npit+1)));
end
